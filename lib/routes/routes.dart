import 'package:auto_route/auto_route_annotations.dart';
import 'package:instagram/feedF/prestation/pages/feed_page.dart';
import 'package:instagram/loginF/prestation/pages/login_page.dart';
import 'package:instagram/loginF/prestation/pages/register_profile_page.dart';
import 'package:instagram/loginF/prestation/pages/splash_screen_page.dart';

// After changes please run:
// `flutter packages pub run build_runner watch`

@MaterialAutoRouter(
  routes: <AutoRoute>[
    // initial route is named "/"
    MaterialRoute(page: SplashScreenPage, initial: true),
    MaterialRoute(page: LoginPage),
    MaterialRoute(page: RegisterProfilePage),
    MaterialRoute(page: FeedPage),
  ],
)
class $InstagramRouter {}
