// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../feedF/prestation/pages/feed_page.dart';
import '../loginF/prestation/pages/login_page.dart';
import '../loginF/prestation/pages/register_profile_page.dart';
import '../loginF/prestation/pages/splash_screen_page.dart';

class Routes {
  static const String splashScreenPage = '/';
  static const String loginPage = '/login-page';
  static const String registerProfilePage = '/register-profile-page';
  static const String feedPage = '/feed-page';
  static const all = <String>{
    splashScreenPage,
    loginPage,
    registerProfilePage,
    feedPage,
  };
}

class InstagramRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.splashScreenPage, page: SplashScreenPage),
    RouteDef(Routes.loginPage, page: LoginPage),
    RouteDef(Routes.registerProfilePage, page: RegisterProfilePage),
    RouteDef(Routes.feedPage, page: FeedPage),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    SplashScreenPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const SplashScreenPage(),
        settings: data,
      );
    },
    LoginPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const LoginPage(),
        settings: data,
      );
    },
    RegisterProfilePage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const RegisterProfilePage(),
        settings: data,
      );
    },
    FeedPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const FeedPage(),
        settings: data,
      );
    },
  };
}
