class FeedConfiguration {
  static const int POSTS_PER_LOAD = 20;
  static const int START_NEW_LOAD_ON_POSTS_LEFT = 10;
}
