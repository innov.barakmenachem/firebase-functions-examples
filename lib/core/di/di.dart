import 'package:get_it/get_it.dart';
import 'package:dartz/dartz.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:instagram/core/entities/user.dart';
import 'package:instagram/loginF/core/failures.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:instagram/core/firebase_entities/collections.dart';

final getIt = GetIt.instance;

void setup() {
  getIt.registerLazySingleton(() => GoogleSignIn());

  getIt.registerSingletonAsync<Either<InstagramUser, UserNotFoundFailure>>(
    () async {
      await Future.delayed(Duration(seconds: 3));
      await Firebase.initializeApp();

      try {
        DocumentSnapshot documentSnapshot = await getIt
            .get<UsersCollection>()
            .collectionReference
            .doc(FirebaseAuth.instance.currentUser.uid)
            .get();

        // documentSnapshot
        getIt.registerLazySingleton(
            () => InstagramUser.fromFirestorUser(documentSnapshot));

        return Left(getIt.get<InstagramUser>());
      } catch (err) {
        print("error occured $err");
        getIt.registerLazySingleton(() => InstagramUser());
        return Right(UserNotFoundFailure());
      }
    },
  );

  getIt.registerLazySingleton(() => UsersCollection());
}
