import 'package:firebase_auth/firebase_auth.dart' as FirebaseAuth;
import 'package:cloud_firestore/cloud_firestore.dart' as cloud_firestore;

class InstagramUser {
  String imageUrl;
  String name;
  String bio;

  InstagramUser({
    this.imageUrl,
    this.name,
    this.bio,
  });

  factory InstagramUser.fromFirestorUser(
      cloud_firestore.DocumentSnapshot userSnapshout) {
    return InstagramUser(
      imageUrl: userSnapshout['profileImageUrl'],
      name: userSnapshout['name'],
      bio: userSnapshout['bio'],
    );
  }
}
