import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:instagram/loginF/prestation/pages/login_page.dart';
import 'package:instagram/loginF/prestation/state_management/login_provider.dart';
import 'package:instagram/loginF/prestation/state_management/register_profile_provider.dart';
import 'package:instagram/feedF/prestation/state_management/feed_provider.dart';
import 'package:instagram/routes/routers_names.dart';
import 'package:instagram/routes/routes.gr.dart';
import 'package:provider/provider.dart';

class InstagramApp extends StatelessWidget {
  InstagramApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<LoginProvider>(create: (_) => LoginProvider()),
        ChangeNotifierProvider<RegisterProfileProvider>(
            create: (_) => RegisterProfileProvider()),
        ChangeNotifierProvider<FeedProvider>(create: (_) => FeedProvider()),
      ],
      child: MaterialApp(
        builder: ExtendedNavigator.builder(
            router: InstagramRouter(), name: RoutesNames.mainNav),
      ),
    );
  }
}
