import 'package:auto_route/auto_route.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:instagram/instagram.dart';
import 'package:instagram/routes/routers_names.dart';
import 'package:instagram/routes/routes.gr.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:instagram/core/di/di.dart' as di;

///
/// Todo:
/// - [v]  Allow user to upload profile image.
/// - [v]  Save the image url in the user firebase model.
/// - [v]  Fix the login process using splash screen.
///
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  di.setup();
  // SharedPreferences prefs = await SharedPreferences.getInstance();
  // prefs.clear();
  runApp(InstagramApp());
}
