import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:instagram/core/constans/feed_configuration.dart';
import 'package:instagram/core/di/di.dart';
import 'package:instagram/core/entities/user.dart';
import 'package:instagram/feedF/domain/entities/post.dart';
import 'package:instagram/routes/routes.gr.dart';
import 'package:instagram/routes/routers_names.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FeedProvider with ChangeNotifier {
  final InstagramUser currentLoggedInUser;
  final GoogleSignIn googleSignIn;

  int postsCounter = 0;
  int refreshCounter = 0;
  List<Post> currentPosts = [];

  FeedProvider()
      : googleSignIn = getIt(),
        currentLoggedInUser = getIt();

  void signOutGoogle() async {
    await googleSignIn.signOut();
    navigateToLoginPage();
    print("User Signed Out");
  }

  Future<bool> listenNewDataAdded() async {
    return await _currentPostsList.stream.any((postsList) {
      return postsList.length > 0;
    });
  }

  final StreamController<List<Post>> _currentPostsList =
      StreamController<List<Post>>.broadcast();
  Stream<List<Post>> get currentPostsStreamList => _currentPostsList.stream;

  Future refreshFeed() async {
    refreshCounter++;
    postsCounter = 0;
    currentPosts.clear();

    callToFirebaseFunctionAlgorithem();
    bool isNewPostsArrive = await listenNewDataAdded();

    if (!isNewPostsArrive) {
      // You can show connection error or other error here
      print("No Results");
    }
  }

  Future callToFirebaseFunctionAlgorithem() async {
    await Future.delayed(Duration(seconds: 2));
    await fetchMorePosts();
  }

  Future fetchMorePosts(
      {int fechedPosts = FeedConfiguration.POSTS_PER_LOAD}) async {
    for (int i = 0; i < fechedPosts; i++) {
      currentPosts.add(
        Post(
          description: "${postsCounter++}. refresh: $refreshCounter",
          photoUrl:
              'https://ehire.co.za/wp-content/uploads/2020/01/birthday-party-ideas-portrait-of-happy-birthday-boy.jpg',
        ),
      );
    }

    print("call done");
    _currentPostsList.sink.add(currentPosts);
  }

  void navigateToLoginPage() {
    ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.loginPage);
  }
}
