import 'package:flutter/material.dart';
import 'package:instagram/feedF/domain/entities/post.dart';

class PostProvider with ChangeNotifier {
  final String likePath = 'lib/core/assets/feed_assets/like.svg';
  final String likeClickedPath = 'lib/core/assets/feed_assets/likeClicked.svg';

  Post post;
  bool isClicked;

  PostProvider({@required this.post}) {
    isClicked = true;
  }

  void like() {
    isClicked = true;
    notifyListeners();
  }
}
