import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:instagram/core/constans/feed_configuration.dart';
import 'package:instagram/core/di/di.dart';
import 'package:instagram/core/entities/user.dart';
import 'package:instagram/core/widgets/instagram_logo.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:cloud_firestore/cloud_firestore.dart' as cloud_firestore;
import 'package:instagram/feedF/domain/entities/post.dart';
import 'package:instagram/feedF/prestation/state_management/post_provider.dart';
import 'dart:async';
import 'package:provider/provider.dart';
import 'package:instagram/feedF/prestation/state_management/feed_provider.dart';
import 'dart:ui' as ui;

class FeedPage extends StatefulWidget {
  const FeedPage({Key key}) : super(key: key);

  @override
  _FeedPageState createState() => _FeedPageState();
}

class _FeedPageState extends State<FeedPage> {
  InstagramUser user;
  _FeedPageState() : user = getIt();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Row(
          children: [
            Container(
              child: ClickableAsset(
                assetPath: 'lib/core/assets/feed_assets/Camera.svg',
                onTap: () {},
                width: 30,
                paddingLeft: 10,
              ),
            ),
            InstagramLogo(h: 50),
          ],
        ),
        leadingWidth: 0,
        actions: [
          ClickableAsset(
            assetPath: 'lib/core/assets/feed_assets/sendMessage.svg',
            onTap: () {},
            width: 30,
            paddingRight: 10,
          ),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: RefreshIndicator(
              onRefresh:
                  Provider.of<FeedProvider>(context, listen: false).refreshFeed,
              child: ListPosts(),
            ),
          ),
          FeedBottomAppBar()
        ],
      ),
    );
  }
}

class ListPosts extends StatefulWidget {
  const ListPosts({
    Key key,
  }) : super(key: key);

  @override
  _ListPostsState createState() => _ListPostsState();
}

class _ListPostsState extends State<ListPosts> {
  StreamController<Post> streamController = StreamController<Post>();
  bool generateNew = false;
  ScrollController _controller;

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      // reach the bottom callback:
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      // reach the top callback
    }
  }

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }

  @override
  void dispose() {
    streamController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    FeedProvider _feedProvider =
        Provider.of<FeedProvider>(context, listen: false);

    return StreamBuilder<List<Post>>(
      stream: _feedProvider.currentPostsStreamList,
      initialData: [],
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) return const Text('no connection');
        if (snapshot.connectionState == ConnectionState.active) {
          return ListView.builder(
            controller: _controller,
            itemCount: snapshot.data.length + 1,
            itemBuilder: (context, index) {
              print(
                  ">>>> BUILDING LIST TILE NO.$index -----------------------------------------");
              print("Total data lenght: ${snapshot.data.length}");
              if (snapshot.data.length -
                      FeedConfiguration.START_NEW_LOAD_ON_POSTS_LEFT ==
                  index) {
                print("PostsListLenght ${_feedProvider.currentPosts.length}");
                print("Snapshot is: ${snapshot.data.length}");
                _feedProvider.callToFirebaseFunctionAlgorithem();
              }
              if (snapshot.data.length == index) {
                return Center(child: CircularProgressIndicator());
              }
              return ChangeNotifierProvider(
                key: ObjectKey(snapshot.data[index]),
                create: (context) => PostProvider(post: snapshot.data[index]),
                child: ListTilePost(),
              );
            },
          );
        } else {
          _feedProvider.callToFirebaseFunctionAlgorithem();
          return CircularProgressIndicator();
        }
      },
    );
  }
}

class ListTilePost extends StatefulWidget {
  const ListTilePost({
    Key key,
  }) : super(key: key);

  @override
  _ListTilePostState createState() => _ListTilePostState();
}

class _ListTilePostState extends State<ListTilePost> {
  @override
  Widget build(BuildContext context) {
    return Consumer<PostProvider>(
      builder: (context, postProvider, child) {
        return ListTile(
          key: UniqueKey(),
          title: Padding(
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: Column(
              children: [
                Stack(
                  alignment: Alignment.center,
                  children: [
                    CachedNetworkImage(
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      imageUrl: postProvider.post.photoUrl,
                    ),
                    Container(
                      color: Color.fromARGB(170, 0, 0, 0),
                      width: 300,
                      child: Center(
                        child: Text(
                          postProvider.post.description,
                          textScaleFactor: 2,
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    ClickableAsset(
                      assetPath: postProvider.isClicked
                          ? postProvider.likePath
                          : postProvider.likeClickedPath,
                      onTap: postProvider.like,
                      width: 25,
                      paddingRight: 10,
                    ),
                    ClickableAsset(
                      assetPath: 'lib/core/assets/feed_assets/writeComment.svg',
                      onTap: () {},
                      width: 25,
                      paddingRight: 10,
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}

class FeedBottomAppBar extends StatelessWidget {
  const FeedBottomAppBar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ClickableAsset(
              assetPath: 'lib/core/assets/feed_assets/homeClicked.svg',
              onTap: () {},
              width: 25,
              paddingRight: 10,
            ),
            ClickableAsset(
              assetPath: 'lib/core/assets/feed_assets/search.svg',
              onTap: () {},
              width: 25,
              paddingRight: 10,
            ),
            ClickableAsset(
              assetPath: 'lib/core/assets/feed_assets/add.svg',
              onTap: () {},
              width: 25,
              paddingRight: 10,
            ),
            ClickableAsset(
              assetPath: 'lib/core/assets/feed_assets/like.svg',
              onTap: () {},
              width: 25,
              paddingRight: 10,
            ),
            ClickableAsset(
              assetPath: 'lib/core/assets/feed_assets/sendMessage.svg',
              onTap: () {},
              width: 25,
              paddingRight: 10,
            ),
          ],
        ),
      ),
    );
  }
}

class ClickableAsset extends StatelessWidget {
  final String assetPath;
  final void Function() onTap;
  final double width;
  final double paddingRight;
  final double paddingLeft;

  const ClickableAsset({
    Key key,
    @required this.assetPath,
    @required this.width,
    this.onTap,
    this.paddingRight,
    this.paddingLeft,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Padding(
        padding:
            EdgeInsets.only(right: paddingRight ?? 0, left: paddingLeft ?? 0),
        child: SvgPicture.asset(
          assetPath,
          width: width ?? 0,
        ),
      ),
      onTap: onTap,
    );
  }
}
