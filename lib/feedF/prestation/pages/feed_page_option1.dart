import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:instagram/core/di/di.dart';
import 'package:instagram/core/entities/user.dart';
import 'package:instagram/core/widgets/instagram_logo.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:cloud_firestore/cloud_firestore.dart' as cloud_firestore;
import 'dart:async';
import 'dart:ui' as ui;

class FeedPage extends StatefulWidget {
  const FeedPage({Key key}) : super(key: key);

  @override
  _FeedPageState createState() => _FeedPageState();
}

class _FeedPageState extends State<FeedPage> {
  InstagramUser user;
  _FeedPageState() : user = getIt();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Row(
          children: [
            Container(
              child: ClickableAsset(
                assetPath: 'lib/core/assets/feed_assets/Camera.svg',
                onTap: () {},
                width: 30,
                paddingLeft: 10,
              ),
            ),
            InstagramLogo(h: 50),
          ],
        ),
        leadingWidth: 0,
        actions: [
          ClickableAsset(
            assetPath: 'lib/core/assets/feed_assets/sendMessage.svg',
            onTap: () {},
            width: 30,
            paddingRight: 10,
          ),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: RefreshIndicator(
              onRefresh: () async {},
              child: ListPosts(),
            ),
          ),
          FeedBottomAppBar()
        ],
      ),
    );
  }
}

class ListPosts extends StatefulWidget {
  const ListPosts({
    Key key,
  }) : super(key: key);

  @override
  _ListPostsState createState() => _ListPostsState();
}

int j = 0;

class Post {
  int i;
  Post(this.i);
}

class _ListPostsState extends State<ListPosts> {
  StreamController<Post> streamController = StreamController<Post>();

  final posts = [];
  // final items2 = List<String>.generate(10000, (i) => "Item $i");

  // int i = 0;
  // Stream<List<Post>> getItems({int bechSize = 10}) async* {
  //   cloud_firestore.FirebaseFirestore.instance.collection('Videos').snapshots();
  //   while (i < 100) {
  //     await Future.delayed(Duration(seconds: 5));
  //     yield Post(i++);
  //   }
  // }

  List<Post> postsOnFirestore = [];
  List<Post> currentPosts = [];
  ScrollController _controller;

  Future<void> fetchMorePosts({int numberOfBatchPosts = 20}) async {
    while (true) {
      for (int i = 0; i < numberOfBatchPosts; i++) {
        streamController.add(Post(j++));
      }
      await Future.delayed(Duration(seconds: 5));
    }
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        // message = "reach the bottom";
        //fetchMorePosts();
      });
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        // message = "reach the top";
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);

    streamController.stream.listen((event) {
      setState(() {
        currentPosts.add(event);
      });
    });

    fetchMorePosts();
  }

  @override
  Widget build(BuildContext context) {
    // StreamBuilder(
    //   stream: snapshot() ,
    //   initialData: [] ,
    //   builder: (BuildContext context, AsyncSnapshot snapshot){
    //     return ListView(snapshot.data)
    //   },
    // ),

    return ListView.builder(
      controller: _controller,
      itemCount: currentPosts.length,
      itemBuilder: (context, index) {
        // memory heap always increased - it keeps every item on the list and not dispose it.
        // TO IMPROVE: keep the data in cache as {index: data}
        return KeepAlive(
            child: ListTileTry(currentPosts: currentPosts, index: index));

        // memory heap has a barrier - it will dispose(remove) items that are not shown on the screen.
        // return ListTileTry(currentPosts: currentPosts, index: index);
      },
    );
  }
}

class KeepAlive extends StatefulWidget {
  const KeepAlive({Key key, this.child}) : super(key: key);

  final Widget child;

  @override
  _KeepAliveState createState() => _KeepAliveState();
}

class _KeepAliveState extends State<KeepAlive>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return widget.child;
  }
}

class ListTileTry extends StatefulWidget {
  const ListTileTry({
    Key key,
    @required this.currentPosts,
    this.index,
  }) : super(key: key);

  final List<Post> currentPosts;
  final int index;

  @override
  _ListTileTryState createState() => _ListTileTryState();
}

class _ListTileTryState extends State<ListTileTry> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20),
        child: Column(
          children: [
            Stack(
              alignment: Alignment.center,
              children: [
                CachedNetworkImage(
                  placeholder: (context, url) => CircularProgressIndicator(),
                  imageUrl:
                      'https://ehire.co.za/wp-content/uploads/2020/01/birthday-party-ideas-portrait-of-happy-birthday-boy.jpg',
                ),
                Container(
                  color: Color.fromARGB(170, 0, 0, 0),
                  width: 200,
                  child: Center(
                    child: Text(
                      '${widget.currentPosts[widget.index].i}',
                      textScaleFactor: 3,
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                ClickableAsset(
                  assetPath: 'lib/core/assets/feed_assets/like.svg',
                  onTap: () {},
                  width: 25,
                  paddingRight: 10,
                ),
                ClickableAsset(
                  assetPath: 'lib/core/assets/feed_assets/writeComment.svg',
                  onTap: () {},
                  width: 25,
                  paddingRight: 10,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}

class FeedBottomAppBar extends StatelessWidget {
  const FeedBottomAppBar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ClickableAsset(
              assetPath: 'lib/core/assets/feed_assets/homeClicked.svg',
              onTap: () {},
              width: 25,
              paddingRight: 10,
            ),
            ClickableAsset(
              assetPath: 'lib/core/assets/feed_assets/search.svg',
              onTap: () {},
              width: 25,
              paddingRight: 10,
            ),
            ClickableAsset(
              assetPath: 'lib/core/assets/feed_assets/add.svg',
              onTap: () {},
              width: 25,
              paddingRight: 10,
            ),
            ClickableAsset(
              assetPath: 'lib/core/assets/feed_assets/like.svg',
              onTap: () {},
              width: 25,
              paddingRight: 10,
            ),
            ClickableAsset(
              assetPath: 'lib/core/assets/feed_assets/sendMessage.svg',
              onTap: () {},
              width: 25,
              paddingRight: 10,
            ),
          ],
        ),
      ),
    );
  }
}

class ClickableAsset extends StatelessWidget {
  final String assetPath;
  final void Function() onTap;
  final double width;
  final double paddingRight;
  final double paddingLeft;

  const ClickableAsset({
    Key key,
    @required this.assetPath,
    @required this.width,
    this.onTap,
    this.paddingRight,
    this.paddingLeft,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Padding(
        padding:
            EdgeInsets.only(right: paddingRight ?? 0, left: paddingLeft ?? 0),
        child: SvgPicture.asset(
          assetPath,
          width: width ?? 0,
        ),
      ),
      onTap: onTap,
    );
  }
}
