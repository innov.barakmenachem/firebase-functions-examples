import 'package:auto_route/auto_route.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:instagram/core/di/di.dart';
import 'package:instagram/core/entities/user.dart';
import 'package:instagram/feedF/prestation/pages/feed_page.dart';
import 'package:instagram/loginF/core/failures.dart';
import 'package:instagram/loginF/core/shared_preferences_names.dart';
import 'package:instagram/loginF/prestation/pages/register_profile_page.dart';
import 'package:instagram/routes/routes.gr.dart';
import 'package:instagram/routes/routers_names.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginProvider with ChangeNotifier {
  final FirebaseAuth _auth;
  final GoogleSignIn googleSignIn;
  final InstagramUser currentLoggedInUser;

  LoginProvider()
      : _auth = FirebaseAuth.instance,
        googleSignIn = getIt(),
        currentLoggedInUser = getIt();

  Future<Either<InstagramUser, LoginFailure>> _signInWithGoogle() async {
    await Firebase.initializeApp();

    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    UserCredential authResult;
    User user;
    if (_auth.currentUser == null) {
      authResult = await _auth.signInWithCredential(credential);
      user = authResult.user;
    } else {
      user = _auth.currentUser;
    }

    if (user != null) {
      assert(!user.isAnonymous);
      assert(await user.getIdToken() != null);

      final User currentUser = _auth.currentUser;
      assert(user.uid == currentUser.uid);

      print('signInWithGoogle succeeded: $user');

      return Left(currentLoggedInUser);
    }

    return Right(LoginFailure());
  }

  Future signInWithGoogle() async {
    Either<InstagramUser, LoginFailure> result = await _signInWithGoogle();

    result.fold(
      (InstagramUser user) async {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        if (prefs.getBool(IS_REGISTERED) == null) {
          navigateToRegisterProfilePage();
        } else {
          navigateToFeedPage();
        }
      },
      (loginFailure) {
        print("Login Failed");
      },
    );
  }

  void navigateToRegisterProfilePage() {
    ExtendedNavigator.named(RoutesNames.mainNav)
        .popAndPush(Routes.registerProfilePage);
  }

  void navigateToFeedPage() {
    ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.feedPage);
  }
}
