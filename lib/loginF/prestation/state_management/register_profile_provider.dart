import 'package:cloud_firestore/cloud_firestore.dart' as cloud_firestore;
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_core/firebase_core.dart' as firebase_core;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:image_picker/image_picker.dart';
import 'package:instagram/core/di/di.dart';
import 'package:instagram/core/entities/user.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:instagram/loginF/core/shared_preferences_names.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:instagram/loginF/core/failures.dart';
import 'package:instagram/loginF/core/success.dart';
import 'package:dartz/dartz.dart';

import 'package:instagram/routes/routes.gr.dart';
import 'package:instagram/routes/routers_names.dart';
import 'package:auto_route/auto_route.dart';
import 'dart:io' as io;

class RegisterProfileProvider with ChangeNotifier {
  final InstagramUser currentLoggedInUser;
  final GlobalKey<FormBuilderState> fbKey = GlobalKey<FormBuilderState>();
  io.File imageFile;

  RegisterProfileProvider() : currentLoggedInUser = getIt();

  Future<void> registerComplete() async {
    if (fbKey.currentState.saveAndValidate()) {
      print(fbKey.currentState.value);

      currentLoggedInUser.name = fbKey.currentState.value['Name'];
      currentLoggedInUser.bio = fbKey.currentState.value['Bio'];

      Either<FirebaseDataSaved, FirebaseCommunicationFailure> saveResult =
          await saveUserOnFirestore(currentLoggedInUser);

      saveResult.fold(
        (success) async {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          await prefs.setBool(IS_REGISTERED, true);
          navigateToFeedpage();
        },
        (failed) {},
      );
    }
  }

  void navigateToFeedpage() {
    ExtendedNavigator.named(RoutesNames.mainNav).popAndPush(Routes.feedPage);
  }

  Future<Either<FirebaseDataSaved, FirebaseCommunicationFailure>>
      saveUserOnFirestore(InstagramUser userToSave) async {
    cloud_firestore.CollectionReference users =
        cloud_firestore.FirebaseFirestore.instance.collection('Users');

    try {
      await users.doc(FirebaseAuth.instance.currentUser.uid).set({
        'bio': userToSave.bio,
        'name': userToSave.name,
        'profileImageUrl': userToSave.imageUrl,
      });
    } catch (err) {
      print('save user failed $err');
      return Right(FirebaseCommunicationFailure());
    }

    if (imageFile != null) {
      await updateUserProfilePhoto(await uploadFile(imageFile));
    }

    return Left(FirebaseDataSaved());
  }

  Future handleUploadType() async {
    PickedFile pickedImage =
        await ImagePicker().getImage(source: ImageSource.gallery);

    if (pickedImage != null) {
      imageFile = io.File(pickedImage.path);
    }
    notifyListeners();
  }

  Future updateUserProfilePhoto(
      firebase_storage.UploadTask photoTaskCompleted) async {
    String photoUrl = await photoTaskCompleted.snapshot.ref.getDownloadURL();
    currentLoggedInUser.imageUrl = photoUrl;

    await cloud_firestore.FirebaseFirestore.instance
        .collection('Users')
        .doc(FirebaseAuth.instance.currentUser.uid)
        .update({'profileImageUrl': photoUrl});
  }

  Future<firebase_storage.UploadTask> uploadFile(io.File file) async {
    firebase_storage.UploadTask uploadTask;
    firebase_storage.Reference ref = firebase_storage.FirebaseStorage.instance
        .ref()
        .child('users_private_media')
        .child('/profile-image-${FirebaseAuth.instance.currentUser.uid}.jpg');

    final metadata = firebase_storage.SettableMetadata(
      contentType: 'image/jpeg',
      customMetadata: {
        'picked-file-path': file.path,
        'user-id': FirebaseAuth.instance.currentUser.uid,
      },
    );

    if (kIsWeb) {
      uploadTask = ref.putData(await file.readAsBytes(), metadata);
    } else {
      uploadTask = ref.putFile(io.File(file.path), metadata);
    }

    return Future.value(uploadTask);
  }
}
