import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:instagram/core/widgets/instagram_logo.dart';
import 'package:instagram/loginF/prestation/state_management/register_profile_provider.dart';
import 'package:provider/provider.dart';

class RegisterProfilePage extends StatelessWidget {
  const RegisterProfilePage({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      key: Provider.of<RegisterProfileProvider>(context, listen: false).fbKey,
      initialValue: {},
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: InstagramLogo(h: 50),
          centerTitle: true,
        ),
        body: Container(
          child: Column(
            children: [
              PhotoUploader(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: TextFieldsEditor(),
              ),
              MaterialButton(
                child: Text("Save"),
                onPressed:
                    Provider.of<RegisterProfileProvider>(context, listen: false)
                        .registerComplete,
              )
            ],
          ),
        ),
      ),
    );
  }
}

class TextFieldsEditor extends StatelessWidget {
  const TextFieldsEditor({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InstagramTextFields(name: "Name"),
        InstagramTextFields(name: "Bio", multiline: true),
      ],
    );
  }
}

class InstagramTextFields extends StatelessWidget {
  final String name;
  final bool multiline;
  const InstagramTextFields({Key key, this.multiline = false, this.name})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(name),
        Expanded(
          child: FormBuilderTextField(
            attribute: name,
          ),
        ),
      ],
    );
  }
}

class PhotoUploader extends StatelessWidget {
  const PhotoUploader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black12,
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 10),
          InkWell(
            child: CirclePhoto(),
            onTap: () async => await Provider.of<RegisterProfileProvider>(
                    context,
                    listen: false)
                .handleUploadType(),
          ),
          SizedBox(height: 10),
          InkWell(
            child:
                Text('Upload User Photo', style: TextStyle(color: Colors.blue)),
            onTap: () async => await Provider.of<RegisterProfileProvider>(
                    context,
                    listen: false)
                .handleUploadType(),
          ),
          SizedBox(height: 10),
        ],
      ),
    );
  }
}

class CirclePhoto extends StatelessWidget {
  final String url;
  final double raduis = 80;
  final Color circleEmptyColor = Colors.white;
  const CirclePhoto({Key key, this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<RegisterProfileProvider>(
      builder: (context, registerProfileProvider, child) {
        if (registerProfileProvider.imageFile == null) {
          return CircleAvatar(
            backgroundColor: circleEmptyColor,
            radius: raduis,
            child: Icon(
              Icons.file_upload,
              size: 50,
              color: Colors.black,
            ),
          );
        } else {
          return CircleAvatar(
            backgroundColor: circleEmptyColor,
            radius: raduis,
            backgroundImage: FileImage(registerProfileProvider.imageFile),
          );
        }
      },
    );
  }
}
