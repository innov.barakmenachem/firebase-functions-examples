import 'package:flutter/material.dart';
import 'package:instagram/core/di/di.dart';
import 'package:auto_route/auto_route.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:instagram/core/di/di.dart';
import 'package:instagram/core/entities/user.dart';
import 'package:instagram/loginF/core/failures.dart';
import 'package:instagram/routes/routers_names.dart';
import 'package:instagram/routes/routes.gr.dart';
import 'package:instagram/loginF/prestation/state_management/login_provider.dart';
import 'package:provider/provider.dart';

class SplashScreenPage extends StatelessWidget {
  const SplashScreenPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    getIt
        .getAsync<Either<InstagramUser, UserNotFoundFailure>>()
        .then((userOrFail) {
      userOrFail.fold(
          (instagramUser) => {
                Provider.of<LoginProvider>(context, listen: false)
                    .signInWithGoogle()
              },
          (userNotFoundFailure) => {
                ExtendedNavigator.named(RoutesNames.mainNav)
                    .popAndPush(Routes.loginPage)
              });
    });

    return Scaffold(
      body: Center(
        child: Image.asset(
          'lib/core/assets/instagram_app_splash.png',
          width: 100,
        ),
      ),
    );
  }
}
