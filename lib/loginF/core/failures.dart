import 'package:instagram/core/failure.dart';

class LoginFailure extends Failure {}

class UserNotFoundFailure extends Failure {}

class FirebaseCommunicationFailure extends Failure {}
